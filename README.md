# PLANTILLA BOOTSTRAP DJ001

En este ejemplo se realizará una plantilla simple con el uso de HTML5, CSS y BOOTSTRAP.

### Pre-requisitos 📋

Requieres un editor de codigo como Visual Code o Notepad++ u otro. 

## Autor ✒️

Ing. Jaime Zambrana Chacón PhD.

Docente Universitario UPDS
También puedes mirar mi web http://jaimezambranachacon.info

## Licencia 📄

Este proyecto está bajo la Licencia (MIT)

## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Da las gracias públicamente 🤓.

---
⌨️ con ❤️ por Ing. Jaime Zambrana Chacón PhD.